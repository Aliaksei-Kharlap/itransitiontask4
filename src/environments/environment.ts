// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
  apiKey: "AIzaSyAV5MgQMhloc3eI8j_ecgVadAsrRHedgzk",
  authDomain: "itransitiontask4-8f2e3.firebaseapp.com",
    projectId: "itransitiontask4-8f2e3",
  storageBucket: "itransitiontask4-8f2e3.appspot.com",
  messagingSenderId: "904626556378",
  appId: "1:904626556378:web:88319c0b304e32824544e3"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
